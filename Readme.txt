-- SUMMARY --

Commerce Zaakpay is a [Drupal Commerce](https://drupal.org/project/commerce) module that integrates
the [Zaakpay](http://zaakpay.com/) payment gateway into your Drupal Commerce shop.

-- REQUIREMENTS --

To use this module you need to signup as a merchant with Zaakpay. To know more how to signup, please visit
https://www.zaakpay.com/contactus

Once you are a merchant with Zaakpay, you will receive a merchant id, which
will be used on your Drupal commerce store.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
* To change the Merchant Id and payment mode
  (Test transactions or Live transactions), Go to
  Store settings > Payment methods admin/commerce/config/payment-methods,
  Look for Zaakpay from the available payment methods and click on edit link.
  Under "Enable payment method: Zaakpay” Action, click on the edit link. Enter
  Merchant Id and Select Payment mode as Live transactions for Zaakpay to
  accept real payments.
